import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const [showEdition, setShowEdition] = useState(false);
  const [name, setName] = useState('');
  const [gender, setGender] = useState('');
  const [phone, setPhone] = useState('');
  const [birthday, setBirthday] = useState('');
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const saveUser = () => {
    save({
      email: data.email,
      name, 
      gender,
      phone,
      birthday
    })
  }

  useEffect(() => {
    load();
  }, []);
  
  useEffect(() => {
    if(data) {
      setName(data.name);
      setPhone(data.phone);
      setGender(data.gender);
      setBirthday(data.birthday);
    }    
  }, [data])
 
  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => setShowEdition((previous) => !previous)}>
            Edit
          </Button>,
        ]}
      >
        {data &&
          (showEdition ? (
            <>
              <Input value={name} onChange={(e) => setName(e.target.value)} name="Name" placeholder="Name" />
              <Input value={gender} onChange={(e) => setGender(e.target.value)} name="Gender" placeholder="Gender" />
              <Input value={phone} onChange={(e) => setPhone(e.target.value)} name="Phone" placeholder="Phone" />
              <Input value={birthday} onChange={(e) => setBirthday(e.target.value)} name="Birthday" placeholder="Birthday" />  
              <Button type="default" onClick={() => saveUser()}>
               Save user
              </Button>            
            </>
          ) : (
            <Descriptions size="small" column={1}>
              <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
              <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
              <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>
              <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
            </Descriptions>
          ))}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
